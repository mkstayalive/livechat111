Messages = new Meteor.Collection("messages");

Template.chatWindow.messages = function() {
	return Messages.find({})
}

function chatWindowScroll() {
	var objDiv = document.getElementById("messagesBlock");
	setTimeout(function() {
		objDiv.scrollTop = objDiv.scrollHeight;
	}, 200);
}

Meteor.startup(function() {
	chatWindowScroll();
})

Template.chatWindow.events({
	'submit #chatForm': function(e, tpl) {

		console.log("Event caught");

		var from = tpl.find(".msg-from").value;
		var body = tpl.find(".msg-body").value;
		Messages.insert({ from: from, body: body, timestamp: Date.now() });
		tpl.find(".msg-body").value = "";
		chatWindowScroll();
		return false;
	}
})